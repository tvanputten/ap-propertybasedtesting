# A simple demonstration of how py.test works to help transition from other testing frameworks
import pytest

def func(x):
    return x + 1

def test_basic():
    assert func(3) == 4

def f():
    raise SystemExit(1)

def test_mytest():
    with pytest.raises(SystemExit):
        f()

@pytest.mark.randomize(i1=int, ncalls=10)
def test_propertybased_basic(i1):
    assert func(i1) == i1+1