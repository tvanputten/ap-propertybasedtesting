# A simple demonstration of how py.test works to help transition from other testing frameworks

import pytest
from pytest import list_of

#import local files
import sys   
sys.path.insert(0, '') 
from btree import BTree

def checkBTree(bt):
	prev = -sys.maxint - 1;
	for i, item in enumerate(bt):
		assert item >= prev
		prev = item

@pytest.mark.randomize(i1=int, ncalls=10)
def test_unittest_btree(i1):
	bt = BTree(2)
	l = range(20, 0, -1)
	bt.insert(8)
	bt.insert(20)
	bt.insert(9)
	bt.insert(11)
	bt.insert(15)
	checkBTree(bt)


@pytest.mark.randomize(l=list_of(int), ncalls=10)
def test_propertytest_btree(l):
	bt = BTree(2)
	for i, item in enumerate(l):
		bt.insert(item)
	checkBTree(bt)
