#Overview

#Setup
This demo project is test against python 2.7.5
Make sure you have virtualenv for python installed globally

#Usage
From the bin folder with virtualenv active
py.test ../<filename of the test you want to run>

#Addtional help
http://pytest.org/latest/getting-started.html#getstarted
https://pypi.python.org/pypi/pytest-quickcheck
