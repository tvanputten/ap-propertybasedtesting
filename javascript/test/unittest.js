var assert = require("assert")

//Basic Unit test to show simple javascript example

describe('Array', function(){
  describe('#indexOf()', function(){
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    })
    it('should return 1 when the value is present at position 1', function(){
      assert.equal(1, [1,2,3].indexOf(2));
    })
  })
})