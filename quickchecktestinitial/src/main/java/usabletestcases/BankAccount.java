package usabletestcases;

/**
 * Created by Ahmet on 2014-10-05.
 */
public class BankAccount {
    public BankAccount(double balance) {
        money = balance;
    }

    private double money;

    public double getAmount() {
        return money;
    }

    public BankAccount add(double amount) {
        //todo: amount cannot be negative
        money += amount;
        return this;
    }

    public BankAccount withdraw(double amount) {
        //todo: amount cannot be negative
        money -= amount;
        return this;
    }
}
