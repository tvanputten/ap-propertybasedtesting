package usabletestcases;

/**
 * Created by Ahmet on 10/1/2014.
 */
public class Person {
    private String firstName;
    private String lastName;
    BankAccount bankAccount;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        bankAccount = new BankAccount(0);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void changeName(String firstName, String lastName) {
        if ( firstName != null )
            this.firstName = firstName;
        if ( lastName != null )
            this.lastName = lastName;
    }
}
