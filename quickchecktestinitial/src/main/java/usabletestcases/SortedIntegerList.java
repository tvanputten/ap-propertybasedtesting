package usabletestcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortedIntegerList {

    private List<Integer> list;

    public SortedIntegerList(List<Integer> list) {
        this.list = new ArrayList<Integer>(list);
        Collections.sort(this.list);
    }

    public List<Integer> toList() {
        return list;
    }
}
