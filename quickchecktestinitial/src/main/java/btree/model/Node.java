package btree.model;

import java.util.ArrayList;
import java.util.Collections;

public class Node implements Comparable<Node>{
	public static Node root;
	private Node parent;
	private ArrayList<Integer> values;
	private ArrayList<Node> children;
	
	public Node(Node parent, ArrayList<Integer> values){
		this.parent = parent;
		children = new ArrayList<Node>();
		Collections.sort(children);
		this.values = values;
	}
	public Node(Node parent ){
		this(parent,new ArrayList<Integer>());
	}
	
	public Node(Node parent,Integer value){
		this(parent);
		values.add(value);
	}
	
	public void inject(Integer value){
		values.add(value);
		Collections.sort(values);
		if(values.size() > 2){
			Integer middle = values.get(1);
			Node right;
			if(parent == null){
				Node parent = new Node(null,values.get(1));
				right =  new Node(parent,values.get(2));
				setParent(parent);
				values.remove(2);
				values.remove(1);
				parent.addChild(this);
				parent.addChild(right);
				Node.root = parent;//HAckzz
			}else{
				right =  new Node(parent,values.get(2));
				values.remove(2);
				values.remove(1);
				parent.addChild(right);
				parent.inject(middle);
				
				
			}
			if(children.size() == 4){
				
				right.addChild(children.get(3));
				children.get(3).setParent(right);
				removeChild(3);
				
				right.addChild(children.get(2));
				children.get(2).setParent(right);
				removeChild(2);
			}
			
		}
		
	}
	
	public void transport(Integer value){
		if(children.size() > 0){
			move(value);
		}else{
			inject(value);
		}
		
	}
	
	private void move(Integer value){
		if(values.size() == 1){
			if(values.get(0) > value){
				children.get(0).transport(value);
			}else{
				children.get(1).transport(value);
			}
		}else if(values.size() == 2){
			if(values.get(0) > value){
				children.get(0).transport(value);
			}else{
				if(values.get(1) > value){
					children.get(1).transport(value);
				}
				else{
					children.get(2).transport(value);
				}
			}
		}
	}
	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public ArrayList<Node> getChildren() {
		return children;
	}
	public Node getLeftChild(){
		if(children.size()>0)
			return children.get(0);
		return null;
	}
	public Node getRightChild(){
		if(children.size()>1)
			return children.get(1);
		return null;
	}
	public void addChild(Node child){
		children.add(child);
		Collections.sort(children);
	}
	
	public void removeChild(int i){
		children.remove(i);
	}

	public ArrayList<Integer> getValues() {
		return values;
	}
	
	public int getTotalValue(){
		Collections.sort(values);
		return values.get(0);
	}
	public int getLowestValue(){
		Collections.sort(values);
		return values.get(0);
	}
	public int getHighestValue(){
		Collections.sort(values);
		if(values.size()>1)
			return values.get(1);
		else
			return values.get(0);
	}
	
	public int compareTo(Node o) {
		return this.getTotalValue()-o.getTotalValue();
	}
}
