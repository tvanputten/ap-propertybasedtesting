package btree.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import btree.model.Node;

public class TreeReader {
	
	private TreeReader(){
		
	}

	public static Node read(String fileName) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(fileName));
		 
		JSONObject jsonObject = (JSONObject) obj;
		System.out.println("File read");
		System.out.println(jsonObject.toJSONString());
		
		
		Node root = new Node(null);
		addValuesToNode(root, jsonObject);
		appendNode(root, (JSONArray)jsonObject.get("childeren"));
		return root;
	}
	
	private static void appendNode(Node n, JSONArray childeren) {
		for(int i = 0; i < childeren.size(); i++){
			Node b = new Node(n);
			n.addChild(b);
			JSONObject json = (JSONObject) childeren.get(i);
			addValuesToNode(b, json);
			appendNode(b, (JSONArray) json.get("childeren"));
		}
	}

	private static void addValuesToNode(Node n, JSONObject obj){
		JSONArray arr = (JSONArray) obj.get("values");
		for(int i = 0; i < arr.size(); i++){
			Long val = (Long) arr.get(i);
			n.inject(val.intValue());
		}
	}

}
