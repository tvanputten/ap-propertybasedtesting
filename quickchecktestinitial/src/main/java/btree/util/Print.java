package btree.util;

import btree.model.Node;



public class Print {
	private Print(){
		
	}
	public static void print(Node n){
		printBranch(n,"");
	}
	private static void printBranch(Node n, String branch){
		if(n!=null){
			System.out.println(branch+nodeToValue(n));
			branch += "\t";
			for(Node c:n.getChildren()){
				printBranch(c, branch);
			}
		}
	}
	private static String nodeToValue(Node n){
		String s = "(";
		for(Integer i :n.getValues()){
			s += i+",";
		}
		return s+")";
	}
}
