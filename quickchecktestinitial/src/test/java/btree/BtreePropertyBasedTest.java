package btree;

import static org.junit.Assert.assertEquals;
import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

import btree.model.Node;
import btree.util.Print;

/**
 * @author Thomas van Putten
 *
 */
public class BtreePropertyBasedTest {

	/**
	 * This tests the full range of the integers possible which the compiler allows to be thrown against the algorithm
	 * This reveals that negative values cause the algorithm to generate invalid results.
	 * A failure like this one show us something we forgot to mention in the original specification.
	 * Failure minimization doesn't appear to do anything in this case?
	 */
	@Test
	public void testFullIntegerRange() {
		Node.root = new Node(null);
		Generator<Integer> generator = PrimitiveGenerators.integers();
		for(int i =1; i< 50;i++){
			Node.root.transport(generator.next());
		}
		Print.print(Node.root);
		/*In this case we expect the tree to be invalid because the algorithme doesn't handle negative values properly */
		assertEquals(false, BtreeUnitTest.evaluateTree(Node.root));
	}
	
	/**
	 * This test does work as expected because it's restricted to positive integers.
	 */
	@Test
	public void testPositiveOnly() {
		Node.root = new Node(null);
		Generator<Integer> generator = PrimitiveGenerators.positiveIntegers();
		for(int i =1; i< 50;i++){
			Node.root.transport(generator.next());
		}
		Print.print(Node.root);
		assertEquals(true, BtreeUnitTest.evaluateTree(Node.root));
	}
}
