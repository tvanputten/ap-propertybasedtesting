package btree;

import static org.junit.Assert.*;

import org.junit.Test;

import btree.model.Node;
import btree.util.Print;

/**
 * @author Thomas van Putten
 *	A more real test to validate if a correct binairy tree is being generated.
 */
public class BtreeUnitTest {

	@Test
	public void test() {
		Node.root = new Node(null);
		for(int i =1; i< 50;i++){
			Node.root.transport(i);
		}
		Print.print(Node.root);
		assertEquals(true, evaluateTree(Node.root));
	}
	
	/**
	 * Validator function to check if a btree is valid
	 * @param n The root node you want to start checking with
	 * @return True if the tree is valid, false if it's NOT valid
	 */
	public static boolean evaluateTree(Node n){
		return evaluateTree(n, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	public static boolean evaluateTree(Node node, int MIN, int MAX) 
	{
	     if(node == null)
	         return true;
	     if(node.getLowestValue() > MIN 
	         && node.getHighestValue() < MAX
	         && evaluateTree(node.getLeftChild(),MIN,node.getHighestValue())
	         && evaluateTree(node.getRightChild(),node.getLowestValue(),MAX))
	         return true;
	     else 
	         return false;
	}
}
