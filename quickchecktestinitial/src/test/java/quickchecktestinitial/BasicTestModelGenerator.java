package quickchecktestinitial;

import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;

public class BasicTestModelGenerator implements
		net.java.quickcheck.Generator<BasicTestModel> {
	Generator<String> names = PrimitiveGenerators.strings();
	Generator<Integer> numbers = PrimitiveGenerators.integers();

	public BasicTestModel next() {
		return new BasicTestModel(names.next(), numbers.next());
	}

}
