package quickchecktestinitial;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Thomas van Putten
 *	This is a normal unit test counterpart to demonstrate how to use Property based unit testing.
 *	In this case a trivially simple model is used but it's just to illustrate the concept.
 */
public class BasicModelRegularUnitTest {

	@Test
	public void testString() {
		BasicTestModel m = new BasicTestModel();
		m.setName("Henk");
		assertEquals("Henk", m.getName());
	}

	@Test
	public void testInt() {
		BasicTestModel m = new BasicTestModel();
		m.setNumber(1);
		assertEquals(1, m.getNumber());
	}

}
