package quickchecktestinitial;

import static org.junit.Assert.assertEquals;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.iterable.Iterables;

import org.junit.Test;

/**
 * @author Thomas van Putten This is a simple demonstration how to use
 *         quickcheck to implement property based unit testing. In this case a
 *         trivially simple model is used but it's just to illustrate the
 *         concept. View the RegularUnitTest for a comparison
 */
public class BasicTestModelPropertyBasedTest {

	@Test
	public void testString() {
		for (int i = 0; i < 1000; i++) {
			BasicTestModel m = new BasicTestModel();
			for (String name : Iterables.toIterable(PrimitiveGenerators
					.strings())) {
				m.setName(name);
				System.out.println(name);
				assertEquals(name, m.getName());
			}
		}
	}

	@Test
	public void testInt() {
		for (int i = 0; i < 1000; i++) {
			BasicTestModel m = new BasicTestModel();
			for (int number : Iterables.toIterable(PrimitiveGenerators
					.integers())) {
				m.setNumber(number);
				assertEquals(number, m.getNumber());
			}
		}
	}

	/**
	 * Just for reference a quick demo of how to setup these tests using a
	 * generator instead of manually constructing the object in the test. This
	 * is especially useful for more complex objects, so you can recycle the
	 * generator across multiple tests.
	 */
	@Test
	public void testWithGenerator() {
		for (BasicTestModel basicModel : Iterables
				.toIterable(new BasicTestModelGenerator())) {
			BasicTestModel newModel = new BasicTestModel(basicModel.getName(),
					basicModel.getNumber());
			assertEquals(basicModel.getName(), newModel.getName());
			assertEquals(basicModel.getNumber(), newModel.getNumber());
		}
	}
}
