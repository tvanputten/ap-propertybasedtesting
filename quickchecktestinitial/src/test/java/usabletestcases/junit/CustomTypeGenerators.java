package usabletestcases.junit;

import org.junit.Test;
import usabletestcases.BankAccount;
import usabletestcases.Person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Ahmet on 10/1/2014.
 */
public class CustomTypeGenerators {
    @Test
    public void newPersonCreation() {
        String firstName = "Jack";
        String lastName = "Bane";

        Person p = new Person(firstName, lastName);

        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));
    }

    @Test
    public void personNameChange() {
        String firstName = "Jack";
        String lastName = "Bane";

        Person p = new Person(firstName, lastName);

        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

        firstName = "Jane";
        p.changeName(firstName, null);
        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

        lastName = "Doe";
        p.changeName(null, lastName);
        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

        firstName = "Some";
        lastName = "One";
        p.changeName(firstName, lastName);
        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

        p.changeName(null, null);
        assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));
    }

    @Test
    public void bankAccountAdditions() {
        Person p = new Person("Name", "Surname");
        BankAccount b = p.getBankAccount();
        double moneyToAdd = 100;
        double delta = 0;

        assertTrue(b.getAmount() == 0);

        assertEquals(b.getAmount() + moneyToAdd, b.add(moneyToAdd).getAmount(), delta);

        moneyToAdd = 12.94;
        assertEquals(b.getAmount() + moneyToAdd, b.add(moneyToAdd).getAmount(), delta);

        moneyToAdd = -10.23;
        assertEquals(b.getAmount() + moneyToAdd, b.add(moneyToAdd).getAmount(), delta);
    }

    @Test
    public void bankAccountWithdrawals() {
        Person p = new Person("Name", "Surname");
        BankAccount b = p.getBankAccount();
        double moneyToWithdraw = 100;
        double delta = 0;

        assertTrue(b.getAmount() == 0);

        assertEquals(b.getAmount() - moneyToWithdraw, b.withdraw(moneyToWithdraw).getAmount(), delta);

        moneyToWithdraw = 12.94;
        assertEquals(b.getAmount() - moneyToWithdraw, b.withdraw(moneyToWithdraw).getAmount(), delta);

        moneyToWithdraw = -10.23;
        assertEquals(b.getAmount() - moneyToWithdraw, b.withdraw(moneyToWithdraw).getAmount(), delta);
    }
}
