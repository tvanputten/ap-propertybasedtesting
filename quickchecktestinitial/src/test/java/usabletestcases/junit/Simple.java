package usabletestcases.junit;

import org.junit.Test;
import usabletestcases.SortedIntegerList;
import usabletestcases.base.SimpleBase;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static usabletestcases.base.SimpleBase.createRandomIntLists;
import static usabletestcases.base.SimpleBase.isSortedList;

public class Simple {
    @Test
    public void sortedIntegerListCreationMultipleTest() {
        for ( List<Integer> intList : createRandomIntLists(10, 20) ) {
            SortedIntegerList sortedList = new SortedIntegerList(intList);
            assertTrue(isSortedList(sortedList.toList()));
        }
    }

    @Test
    public void sortedIntegerListCreation() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(8);
        list.add(10);
        list.add(1);
        list.add(-1);

        SortedIntegerList sortedList = new SortedIntegerList(list);
        List<Integer> expected = SimpleBase.createSortedIntegerList(list);
        assertEquals(expected, sortedList.toList());
    }

    @Test
    public void sortedIntegerListCreationLoopCheck() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(8);
        list.add(10);
        list.add(1);
        list.add(-1);
        list.add(-1);

        SortedIntegerList sortedList = new SortedIntegerList(list);

        assertTrue(isSortedList(sortedList.toList()));
    }

    @Test
    public void sortedIntegerListCreationManualCheck() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(8);
        list.add(10);
        list.add(1);
        list.add(-1);

        List<Integer> expectedList = new ArrayList<Integer>();
        expectedList.add(-1);
        expectedList.add(1);
        expectedList.add(8);
        expectedList.add(10);

        SortedIntegerList sortedList = new SortedIntegerList(list);
        assertEquals(expectedList, sortedList.toList());
    }
}
