package usabletestcases.quickcheck;

import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.iterable.Iterables;
import org.junit.Test;
import usabletestcases.BankAccount;
import usabletestcases.Person;
import usabletestcases.base.PersonGenerator;
import usabletestcases.base.PersonGeneratorMocked;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

/**
 * Created by Ahmet on 10/1/2014.
 */
public class CustomTypeGenerators {
    @Test
    public void newPersonCreation() {
        PersonGenerator pg;
        for ( Person p : Iterables.toIterable(pg = new PersonGenerator()) ) {
            assertTrue(p.getFirstName().equals(pg.FirstName) && p.getLastName().equals(pg.LastName));
            // or something like:
            // assertEquals(p, new Person(p.getFirstName(), p.getLastName()));
            // Requires IComparable though...
        }
    }

    @Test
    public void personNameChange() {
        PersonGenerator pg;
        for ( Person p : Iterables.toIterable(pg = new PersonGenerator()) ) {
            String firstName = pg.FirstName;
            String lastName = pg.LastName;

            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            PersonGenerator pg1 = new PersonGenerator();
            pg1.next();
            firstName = pg1.FirstName;
            p.changeName(firstName, null);
            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            lastName = pg1.LastName;
            p.changeName(null, lastName);
            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            PersonGenerator pg2 = new PersonGenerator();
            pg2.next();
            firstName = pg2.FirstName;
            lastName = pg2.LastName;
            p.changeName(firstName, lastName);
            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            p.changeName(null, null);
            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));
        }
    }

    @Test
    public void personNameChangeMocking() {
        PersonGeneratorMocked pg;
        for ( Person p : Iterables.toIterable(pg = new PersonGeneratorMocked()) ) {
            String firstName = pg.FirstName;
            String lastName = pg.LastName;

            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            PersonGenerator pg1 = new PersonGenerator();
            pg1.next();
            firstName = pg1.FirstName;
            //doCallRealMethod().when(p).changeName(firstName, null);

            p.changeName(firstName, null);
            verify(p).changeName(firstName, null);
            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            lastName = pg1.LastName;

            p.changeName(null, lastName);
            verify(p).changeName(null, lastName);

            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            PersonGenerator pg2 = new PersonGenerator();
            pg2.next();
            firstName = pg2.FirstName;
            lastName = pg2.LastName;

            p.changeName(firstName, lastName);
            verify(p).changeName(firstName, lastName);

            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));

            p.changeName(null, null);
            verify(p).changeName(null, null);

            assertTrue(p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));
        }
    }

    @Test
    public void bankAccountAdditions() {
        Person p = new Person("Name", "Surname");
        BankAccount b = p.getBankAccount();
        double moneyToAdd;
        double delta = 0;

        assertTrue(b.getAmount() == 0);

        for ( Double i : Iterables.toIterable(PrimitiveGenerators.doubles()) ) {
            moneyToAdd = i;
            assertEquals(b.getAmount() + moneyToAdd, b.add(moneyToAdd).getAmount(), delta);
        }
    }

    @Test
    public void bankAccountWithdrawals() {
        Person p = new Person("Name", "Surname");
        BankAccount b = p.getBankAccount();
        double moneyToWithdraw;
        double delta = 0;

        assertTrue(b.getAmount() == 0);

        for ( Double i : Iterables.toIterable(PrimitiveGenerators.doubles()) ) {
            moneyToWithdraw = i;
            assertEquals(b.getAmount() - moneyToWithdraw, b.withdraw(moneyToWithdraw).getAmount(), delta);
        }
    }
}
