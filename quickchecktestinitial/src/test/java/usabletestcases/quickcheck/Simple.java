package usabletestcases.quickcheck;

import org.junit.Test;
import usabletestcases.SortedIntegerList;
import usabletestcases.base.SimpleBase;

import java.util.List;

import static net.java.quickcheck.generator.CombinedGeneratorsIterables.someLists;
import static net.java.quickcheck.generator.PrimitiveGenerators.integers;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static usabletestcases.base.SimpleBase.isSortedList;

public class Simple {
    @Test
    public void sortedIntegerListCreation() {
        for ( List<Integer> intList : someLists(integers()) ) {
            SortedIntegerList sortedList = new SortedIntegerList(intList);

            List<Integer> expected = SimpleBase.createSortedIntegerList(intList);
            assertEquals(expected, sortedList.toList());
        }
    }

    @Test
    public void sortedIntegerListCreationLoopCheck() {
        for ( List<Integer> intList : someLists(integers()) ) {
            SortedIntegerList sortedList = new SortedIntegerList(intList);
            assertTrue(isSortedList(sortedList.toList()));
        }
    }
}
