package usabletestcases.base;

import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;
import usabletestcases.Person;

/**
 * Created by Ahmet on 10/1/2014.
 */
public class PersonGenerator implements net.java.quickcheck.Generator<Person> {
    Generator<String> firstName = PrimitiveGenerators.strings();
    Generator<String> lastName = PrimitiveGenerators.strings();

    public String FirstName;
    public String LastName;

    @Override
    public Person next() {
        FirstName = firstName.next();
        LastName = lastName.next();

        return new Person(FirstName, LastName);
    }
}