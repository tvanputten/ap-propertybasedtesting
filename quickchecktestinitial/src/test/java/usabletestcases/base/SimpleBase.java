package usabletestcases.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SimpleBase {
    public static List<Integer> createSortedIntegerList(List<Integer> any) {
        ArrayList<Integer> sorted = new ArrayList<Integer>(any);
        Collections.sort(sorted);
        return sorted;
    }


    public static boolean isSortedList(List<Integer> list) {
        if ( list.size() == 1 )
            return true;

        for ( int i = 0; i < list.size(); i++ ) {
            if ( i == list.size() - 1 ) {
                if ( !(list.get(i - 1) <= list.get(i)) )
                    return false;
                break;
            }

            if ( !(list.get(i) <= list.get(i + 1)) )
                return false;
        }

        return true;
    }

    static Random rand = new Random();

    public static List<Integer> createRandomIntList(int amount) {
        ArrayList<Integer> list = new ArrayList<Integer>();

        for ( int i = 0; i < amount; i++ ) {
            list.add(rand.nextInt());
        }

        return list;
    }

    public static List<List<Integer>> createRandomIntLists(int listAmount, int amountPerList) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        for ( int i = 0; i < listAmount; i++ ) {
            lists.add(createRandomIntList(amountPerList));
        }
        return lists;
    }
}
