package usabletestcases.base;

import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;
import org.mockito.Mockito;
import usabletestcases.Person;

import static org.mockito.Mockito.mock;

/**
 * Created by Ahmet on 2014-11-24.
 */
public class PersonGeneratorMocked implements net.java.quickcheck.Generator<Person> {
    Generator<String> firstName = PrimitiveGenerators.strings();
    Generator<String> lastName = PrimitiveGenerators.strings();

    public String FirstName;
    public String LastName;

    @Override
    public Person next() {
        FirstName = firstName.next();
        LastName = lastName.next();

        Person p = mock(Person.class, Mockito.CALLS_REAL_METHODS);
        p.changeName(FirstName, LastName);
        return p;
    }
}
