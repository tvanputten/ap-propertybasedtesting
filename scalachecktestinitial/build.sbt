name := """PropertyBasedTestingexample"""

organization := "example"

version := "0.1.0-SNAPSHOT"

//scalaVersion := "2.11.2"

//crossScalaVersions := Seq("2.10.4", "2.11.2")
autoCompilerPlugins := true

libraryDependencies ++= Seq(
  "org.scala-tools" % "maven-scala-plugin" % "2.15.2",
  "org.apache.maven.plugins" % "maven-eclipse-plugin" % "2.9",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.9" % "test",
  "junit" % "junit" % "4.11" % "test",
  "com.novocode" % "junit-interface" % "0.11" % "test",
  "commons-lang" % "commons-lang" % "2.5",
  "org.scala-lang" % "jline" % "2.9.0-1"
)

testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v")

initialCommands := "import example._"

