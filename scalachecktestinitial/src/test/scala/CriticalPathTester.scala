

package scala;

/**
 * @author Brett
 * 
 * Copied the BasicTestModelTest.scala from Eric and altered it for chapter 4.5 (at this point)
 * to test the criticalpath calculator.
 */

import criticalPath._
import scala.collection.mutable.HashSet
import org.scalacheck.Prop._
import org.junit.runner.RunWith
import com.company.junit._
import org.scalacheck.Properties
import com.company.scalacheck.support.ScalaCheckJUnitPropertiesRunner
import com.company.junit.NaamGenerator.arbNaam

/**
 * This is an example of a JUnit test suite implemented as a ScalaCheck Properties object, where all
 * unit test cases are properties that are evaluated as separate JUnit test cases
 *
 * The code is exactly the same as if the property was run using org.scalacheck.Test.check (and in
 * fact it can still be run like that) but using the @RunWith annotation with our custom runner
 * it can also be run as a JUnit suite
 */

@RunWith(classOf[ScalaCheckJUnitPropertiesRunner])
class CriticalPathTestRunnerTest extends Properties("CriticalPath Test Model") {
  import NaamGenerator._

  //This holds true if 
  property("Test Kritieke Pad") = forAll { (taskName: String, taskDuration: Int) =>
    val calc = new CriticalPathCalculator()
    val task = new Task(taskName, taskDuration)
    val listOfTasks = new HashSet()
    /**
     * Compiled, maar weet niet goed hoe ik hier op de results test of ze kloppen.
     * 't punt is dat er tasks gegenereerd worden, die uiteindelijk een soort work breakdown structure
     * moeten voorstellen. De taskgenerator is geschreven, maar hoe bepaal je welke task van welke 
     * afhankelijk moet zijn? (Cyclic dependency throwt een error.) 
     * Zat te denken aan gewoon 1 hele lange rij voor een simpele test, verder idunno.
     */
   
    //calc.getCriticalPathWithinListOfTasks(listOfTasks)
    
    1 == 1
  }
}