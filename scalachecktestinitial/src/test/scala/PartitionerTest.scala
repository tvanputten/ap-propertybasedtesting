package scala

import partitioner.Partitioner

import com.company.scalacheck.support.ScalaCheckJUnitPropertiesRunner
import org.junit.runner.RunWith
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Properties}

import scala.collection.JavaConversions._
import scala.collection.mutable

@RunWith(classOf[ScalaCheckJUnitPropertiesRunner])
class PartitionerTest extends Properties("Integer Partitioning Test") {
  // http://stackoverflow.com/questions/12640690/recursive-integer-partitioning-algorithm
  def partition(n: Int): Int = {
    def inner(n: Int, max: Int): Int =
      if (n == 0) 1
      else if (n < 0) 0
      else ((1 to max) map (x => inner(n - x, x))).sum

    if (n == 0) 0
    else inner(n, n - 1)
  }

  val partitionValues = Gen.choose(1, 25)

  property("First output test") = forAll(partitionValues) { (n) =>
    val output = new java.util.ArrayList[String]()

    Partitioner.partition(n, n, "", output)

    output.head.toInt == n
  }

  property("Last output test") = forAll(partitionValues) { (n) =>
    val output = new java.util.ArrayList[String]()

    Partitioner.partition(n, n, "", output)

    output.last.split(" \\+ ").map(_.toInt).size == n
  }

  property("Amount of generated summations test") = forAll(partitionValues) { (n) =>
    val output = new java.util.ArrayList[String]()

    Partitioner.partition(n, n, "", output)

    output.size == (partition(n) + 1)
  }

  property("Summation test") = forAll(partitionValues) { (n) =>
    val output = new java.util.ArrayList[String]()

    Partitioner.partition(n, n, "", output)

    output forall { case (str) =>
      val sum = str.split(" \\+ ").map(_.toInt).sum
      sum == n
    }
  }

  property("Duplication test") = forAll(partitionValues) { (n) =>
    val output = new java.util.ArrayList[String]()

    Partitioner.partition(n, n, "", output)

    val result = new mutable.MutableList[Array[Int]]

    output forall { case (str) =>
      val ints = str.split(" \\+ ").map(_.toInt).sorted
      val containsIt = result exists { x => ints.size == x.size && ints.sameElements(x)}
      result += ints
      !containsIt
    }
  }
}