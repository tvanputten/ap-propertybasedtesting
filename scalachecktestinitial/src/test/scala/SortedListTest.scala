

package scala
/**
 * @author Thomas
 */

import sortedlist.SortedList
import org.scalacheck.Prop._
import org.junit.runner.RunWith
import com.company.junit._
import com.company.scalacheck.support.ScalaCheckJUnitPropertiesRunner
import org.scalacheck.{ Gen, Properties }

/**
 * This is an example of a JUnit test suite implemented as a ScalaCheck Properties object, where all
 * unit test cases are properties that are evaluated as separate JUnit test cases
 *
 * The code is exactly the same as if the property was run using org.scalacheck.Test.check (and in
 * fact it can still be run like that) but using the @RunWith annotation with our custom runner
 * it can also be run as a JUnit suite
 */

@RunWith(classOf[ScalaCheckJUnitPropertiesRunner])
class SortedListTestRunnerTest extends Properties("Sorted List Test") {
  // This holds true and will be reported as a passed test by JUnit
  property("Test object maken") = forAll { (naam: String, id: Int) =>
    val list = new SortedList();
    list.add(3);
    list.add(2);
    list.add(1);
    
  }
  def checkOrder(list : SortedList): Boolean = {
  
  
  }
}

