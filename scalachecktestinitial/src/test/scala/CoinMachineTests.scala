

package scala
import coinmachine.Machine
import quickchecktestinitial.BasicTestModel
import org.scalacheck.Prop._
import org.junit.runner.RunWith
import com.company.junit._
import com.company.scalacheck.support.ScalaCheckJUnitPropertiesRunner
import org.scalacheck.{ Gen, Properties }

/**
 * This is an example of a JUnit test suite implemented as a ScalaCheck Properties object, where all
 * unit test cases are properties that are evaluated as separate JUnit test cases
 *
 * The code is exactly the same as if the property was run using org.scalacheck.Test.check (and in
 * fact it can still be run like that) but using the @RunWith annotation with our custom runner
 * it can also be run as a JUnit suite
 */

@RunWith(classOf[ScalaCheckJUnitPropertiesRunner])
class CoinMachineTests extends Properties("CoinMachine Tests") {
  
  property("Verify the amount of money that was given back") = forAll { (amountOfMoney: Int) =>
    //val c = new Machine();
    // Brett: Eerst omgeving opnieuw in elkaar zetten, helemaal naar van alle non-hinting errors.
    1 == 1
  }

  /*
  // This holds true and will be reported as a passed test by JUnit
  property("Test object maken") = forAll { (naam: String, id: Int) =>
    val p = new BasicTestModel(naam, id)
    p.getName == naam && p.getNumber == id
  }

  property("Test zet naam") = forAll { (naam: String) =>
    val p = new BasicTestModel("Thomas", -1)
    p.setName(naam)
    p.getName == naam
  }

  property("Test zet nummer") = forAll { (id: Int) =>
    val p = new BasicTestModel("Thomas", -1)
    p.setNumber(id)
    p.getNumber == id
  } */
}