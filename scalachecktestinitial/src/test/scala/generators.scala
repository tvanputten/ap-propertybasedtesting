package com.company.junit

import kaas.Vierkant
import criticalPath.Task
import org.junit.Assert._
import org.junit.Test
import org.scalacheck.Test.Params
import org.scalacheck.{ConsoleReporter, Prop, Properties, Arbitrary, Gen, Test => SchkTest}
import org.scalacheck.Prop._

/**
 * Generator of case objects for the Rectangle class, as well as an arbitrary generator
 */
object VierkantGenerator {
  // generator for the Rectangle case class
  val vierkantGen: Gen[Vierkant] = for {
    height <- Gen.choose(0, 9999)
    width <- Gen.choose(0, 9999)
  } yield ( new Vierkant(width, height))

  // Arbitrary generator of rectangles
  implicit val arbVierkant: Arbitrary[Vierkant] = Arbitrary(vierkantGen)
}


object NaamGenerator {
  // generator for the Rectangle case class
  val naamGen: Gen[String] = for {
    naam <- Gen.alphaStr
  } yield ( new String(naam))

  // Arbitrary generator of rectangles
  implicit val arbNaam: Arbitrary[String] = Arbitrary(naamGen)
}

object TaskGenerator {
  // generator for the Rectangle case class
  val taskGen: Gen[Task] = for {
    duration <- Gen.choose(0, 9999)
    name <- Gen.alphaStr
    // TODO: Add dependencies: Other task objects.
  } yield ( new Task(name, duration))

  // Arbitrary generator of rectangles
  implicit val arbVierkant: Arbitrary[Task] = Arbitrary(taskGen)
}

