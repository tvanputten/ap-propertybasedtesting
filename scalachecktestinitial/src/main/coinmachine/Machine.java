package coinmachine;

import java.util.ArrayList;
import java.util.Scanner;

public class Machine {
	public MoneyList moneyList;

	public Machine(MoneyList aMoneyList) {
		this.moneyList = aMoneyList;
	}

	public MoneyList getMoneyList() {
		return this.moneyList;
	}

	public ArrayList<Integer> giveBack(int aCentsRemaining) {
		// Initialize giveList and list of money values.
		ArrayList<Integer> giveList = new ArrayList<Integer>();
		ArrayList<Integer> valuesList = this.moneyList.getValuesSortedDesc();

		while (aCentsRemaining > 0) {
			// Check if we still have values to give.
			if (valuesList.size() == 0) {
				// No more values left to give, we ran out of coins!
				return null;
			}

			// Get highest value
			int highestValue = valuesList.get(0);

			// Remove highest value from remaining as long as possible.
			while (aCentsRemaining >= highestValue && this.moneyList.getCountByValue(highestValue) > 0) {
				// Remove highest values.
				aCentsRemaining -= highestValue;

				// Add value to give list.
				giveList.add(highestValue);

				// Remove value from money list.
				this.moneyList.removeFromCountByValue(highestValue, 1);
			}

			// No longer able to remove highest value, remove it from the list.
			valuesList.remove(0);
		}

		// Done!
		return giveList;
	}
	
	public ArrayList<Integer> useMoneyMachine() {
		// Create money list.
		MoneyList moneyList = new MoneyList();

		// Denomination, value, amount
		moneyList.add("1 Cent", 1, 10);
		moneyList.add("2 Cent", 2, 5);
		moneyList.add("5 Cent", 5, 2);
		moneyList.add("10 Cent", 10, 2);
		moneyList.add("20 Cent", 20, 2);
		moneyList.add("50 Cent", 50, 2);
		moneyList.add("1 Euro", 100, 2);
		moneyList.add("2 Euro", 200, 5);

		// Create machine
		Machine machine = new Machine(moneyList);

		// Print money list
		System.out.println("Hoeveelheid munten in automaat:");
		int totalCents = 0;
		for (String name : machine.getMoneyList().getNames()) {
			System.out.println("Munt: " + name + "\t Hoeveelheid: " + machine.getMoneyList().getCount(name));
			totalCents += machine.getMoneyList().getValue(name) * machine.getMoneyList().getCount(name);
		}
		System.out.println("Totale hoeveelheid in automaat: " + totalCents);

		// Get user input of amounts of cents to give back.
		System.out.println("Hoeveelheid centen om terug te geven:");

		Scanner in = new Scanner(System.in);
		int giveBackAmount = in.nextInt();
		in.close();

		// Giveback time!
		ArrayList<Integer> coins = machine.giveBack(giveBackAmount);

		try {
			if(coins == null) {
				throw new Exception("Not enough coins.");
			} else {
				return coins;
			}
		} catch (Exception e) {
			System.out.println("Something went wrong.");
			e.printStackTrace();
		} 
		return null;
	}
}
