package coinmachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;

public class MoneyList {
	private LinkedHashMap<String, Integer> denominations;
	private LinkedHashMap<String, Integer> counts;

	public MoneyList() {
		this.denominations = new LinkedHashMap<String, Integer>();
		this.counts = new LinkedHashMap<String, Integer>();
		
	}

	public void add(String aName, int aValue, int aCount) {
		this.denominations.put(aName, aValue);
		this.counts.put(aName, aCount);
	}

	public ArrayList<String> getNames() {
		ArrayList<String> names = new ArrayList<String>();

		Collection<String> keys = this.denominations.keySet();

		names.addAll(keys);

		return names;
	}

	public int getValue(String aName) {
		return this.denominations.get(aName);
	}

	public String getNameByValue(int aValue) {
		Collection<String> names = this.denominations.keySet();

		for (String name : names) {
			int value = this.denominations.get(name);

			if (value == aValue) {
				return name;
			}
		}

		return null;
	}

	public int getCountByValue(int aValue) {
		String name = this.getNameByValue(aValue);

		return this.getCount(name);
	}

	public void removeFromCountByValue(int aValue, int anAmount) {
		String name = this.getNameByValue(aValue);

		this.counts.put(name, this.counts.get(name) - anAmount);
	}

	public int getCount(String aName) {
		return this.counts.get(aName);
	}

	public ArrayList<Integer> getValuesSortedDesc() {
		ArrayList<Integer> values = new ArrayList<Integer>();

		Collection<String> names = this.denominations.keySet();

		for (String name : names) {
			int value = this.denominations.get(name);

			values.add(value);
		}

		Collections.sort(values, Collections.reverseOrder());

		return values;
	}
}
