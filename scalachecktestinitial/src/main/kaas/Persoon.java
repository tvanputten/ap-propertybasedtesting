package kaas;

public class Persoon {
	private String naam;

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public Persoon(String naam) throws Exception {
		super();
		this.naam = naam;
	}
	
	
}
