package kaas;

public class Vierkant {
	int lengte;
	int breedte;
	
	public Vierkant(int lengte, int breedte) throws Exception {
		super();
		this.lengte = lengte;
		this.breedte = breedte;
	}

	public int getLengte() {
		return lengte;
	}

	public void setLengte(int lengte) {
		this.lengte = lengte;
	}

	public int getBreedte() {
		return breedte;
	}

	public void setBreedte(int breedte) {
		this.breedte = breedte;
	}
	
	public int getOppervlakte(){
		return lengte*breedte;
	}
}
