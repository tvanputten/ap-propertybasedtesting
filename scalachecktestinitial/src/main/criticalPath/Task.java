package criticalPath;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @author Brett
 * 
 * This is a supporting class for the critical path implementation.
 * These are the tasks that are needed to be fulfilled to reached the end.
 * Tasks can be dependent on other tasks.
 */
public class Task {
	// Properties
	public int cost;
	public int criticalCost;
	public String name;
	public HashSet<Task> dependencies = new HashSet<Task>();
	
	// Constructors
	public Task(String name, int cost, Task... dependencies) {
	  this.name = name;
	  this.cost = cost;
	  for(Task t : dependencies){
	    this.dependencies.add(t);
	  }
	}
	
	// Public Methods
	@Override
	public String toString() {
		StringBuffer temp = new StringBuffer("Minimum cost task \'" + name + "\' is " + criticalCost + ". Own cost is " + cost + ". Dependencies: ");
		for(Iterator<Task> it = dependencies.iterator(); it.hasNext(); ) {
			temp.append(it.next().getName() + " ");
		}
		return temp.toString();
	}
	
	public String getName() { 
		return name;
	}
	
	public boolean isDependent(Task t) {
	  // Check for direct dependency
	  if (dependencies.contains(t)) {
	    return true;
	  }
	  // Recursively check for indirect dependency
      for (Task dep : dependencies) {
        if (dep.isDependent(t)){
          return true;
        }
      }
      return false;
    }
	
	// Other Methods
	
 }