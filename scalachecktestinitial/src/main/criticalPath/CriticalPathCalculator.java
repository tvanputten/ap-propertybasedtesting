package criticalPath;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author Brett
 * 
 * This class is a partial implementation of my CriticalPath assignment for the 
 * advanced algorithms course. This class was used in chapter 4.5 (at this point)
 * to demonstrate the difference in workload for the programmer in testing a 
 * piece of code (this class) by normal unit test and pbt testing.
 */
public class CriticalPathCalculator {

	// Properties

	// Constructors
	public CriticalPathCalculator() { 

	}

	// Public Methods

	/**
	 * Returns a list of tasks, with the minimum critical cost stored inside
	 * of the task.
	 * 
	 * @param listOfTasks - A HashSet of tasks that need to have their minimal critical costs resolved.
	 * @return - The same tasks that were given, but in a different order. The tasks now hold their minimal critical costs.
	 */
	public Task[] getListOfResolvedTasks(HashSet<Task> listOfTasks) {
		HashSet<Task> calculated = new HashSet<Task>();
		HashSet<Task> toBeCalculated = new HashSet<Task>(listOfTasks);

		//Backflow algorithm
		while(!toBeCalculated.isEmpty()){
			boolean progress = false;
			
			// Whenever a task's dependencies have not been fully resolved yet, the task is left as is until a next iteration.
			// This is done so that in several iterations all dependencies will be resolved, unless there is a cyclic dependency. See RunTimeException about cyclic.
			for(Iterator<Task> it = toBeCalculated.iterator(); it.hasNext(); ) {
				Task task = it.next();
				if(calculated.containsAll(task.dependencies)){
					// We've already calculated the costs for all dependencies, so find out which one has the highest minimum cost
					int critical = 0;
					for(Task t : task.dependencies){
						if(t.criticalCost > critical){
							critical = t.criticalCost;
						}
					}
					// Now add the cost for this task and add this task to the calculated set.
					task.criticalCost = critical + task.cost;
					calculated.add(task);
					it.remove();
					 
					progress = true;
				}
			}
			//If we haven't made any progress then a cycle must exist in
			//the graph and we wont be able to calculate the critical path
			if(!progress) throw new RuntimeException("Cyclic dependency, algorithm stopped!");
		}
		
		// NOTE: This is the point to get only the critical path if desired - 
		// All costs have been determined, start at the end and work your way back.

		//get the tasks
		Task[] ret = calculated.toArray(new Task[0]);
		//create a priority list
		Arrays.sort(ret, new Comparator<Task>() {

		public int compare(Task o1, Task o2) {
			//sort by cost
			int i= o2.criticalCost-o1.criticalCost;
			if(i != 0)return i;

			//using dependency as a tie breaker
			//note if a is dependent on b then
			//critical cost a must be >= critical cost of b
			if(o1.isDependent(o2))return -1;
			if(o2.isDependent(o1))return 1;
			return 0;
		}
		});

		return ret;
	}

	// Other Methods

}
