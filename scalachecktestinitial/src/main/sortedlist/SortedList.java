package sortedlist;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;

public class SortedList<Integer> extends AbstractList<Integer> {

    private ArrayList<Integer> internalList = new ArrayList<Integer>();

    @Override 
    public void add(int position, Integer i) {
        internalList.add(i);
        if(!internalList.contains(9)){
        	Collections.sort(internalList, null);
        }
    }

    @Override
    public Integer get(int i) {
        return internalList.get(i);
    }

    @Override
    public int size() {
        return internalList.size();
    }

}