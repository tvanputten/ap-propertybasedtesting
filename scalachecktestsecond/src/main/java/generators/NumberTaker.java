package generators;

import java.util.ArrayList;
import java.util.Collections;

public class NumberTaker {
private ArrayList<Integer> numberList;
    
    public NumberTaker(){
        numberList = new ArrayList<Integer>();
    }
    
    public void addNumberToList(int newNumber){
        numberList.add(newNumber);
        Collections.sort(numberList);
    }
    
    public ArrayList<Integer> getNumberList(){
        return numberList;
    }
    
    public boolean orderIsCorrect(){
        boolean orderIsCorrect = true;
        
        for(int i=0; i<numberList.size(); i++){
            if(i != 0){
                if(numberList.get(i-1) > numberList.get(i))
                    orderIsCorrect = false;
            }
        }
        
        return orderIsCorrect;
    }
}