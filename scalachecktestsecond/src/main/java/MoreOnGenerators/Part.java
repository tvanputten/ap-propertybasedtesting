package moregenerators;

public class Part{
    private final String name;
    private final int number;
    
    
    public Part(String name, int number){
        this.name= name;
        this.number = number;
    }
    
    public String getName(){
        return name;
    }
    
    public int getNumber(){
        return number;
    }
}