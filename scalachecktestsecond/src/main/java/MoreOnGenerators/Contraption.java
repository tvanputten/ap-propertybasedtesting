package moregenerators;

import java.util.ArrayList;
import java.util.List;

public class Contraption{
    ArrayList<Part> list;
    
    public Contraption(){
        list = new ArrayList<Part>();
    }
    
    
    public void addPart(Part aPart){
        list.add(aPart);
    }
    
    public List<Part> getList(){
        return list;
    }
}