package wordgenerator;

import java.util.Random;

public class WordGenerator {
	private char[] consonants = new char[] { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
	private char[] vowels = new char[] { 'a', 'e', 'i', 'o', 'u' };

	public static void main(String[] args) {
		String wordSequence = "";

		for(int i = 0; i < 6; i++) {
			wordSequence += "-" + new WordGenerator().getRandomWord(5 + new Random().nextInt(5));
		}

		System.out.println(wordSequence);
	}

	public String getRandomWord(int aLength) {
		StringBuffer newWord = new StringBuffer();

		while (newWord.length() < aLength) {
			if (newWord.length() % 2 == 0) {
				newWord.append(consonants[new Random().nextInt(consonants.length)]);
			} else {
				newWord.append(vowels[new Random().nextInt(vowels.length)]);
			}
		}
		
		return newWord.toString();
	}
}
