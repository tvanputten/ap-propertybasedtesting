/**
 * @author Maurits de Bruin
 * Altered by Brett Meenink, for testing it through Property Based Testing
 */

package coinmachine;

import java.util.ArrayList;
import java.util.Scanner;

public class Machine {
	public MoneyList moneyList;

	public Machine(MoneyList aMoneyList) {
		this.moneyList = aMoneyList;
	}
	
	public MoneyList getMoneyList() {
		return this.moneyList;
	}

	public ArrayList<Integer> giveBack(int amountOfMoneyToGiveBack) {
		// Initialize giveList and list of money values.
		ArrayList<Integer> giveList = new ArrayList<Integer>();
		ArrayList<Integer> valuesList = this.moneyList.getValuesSortedDesc();

		while (amountOfMoneyToGiveBack > 0) {
			// Check if we still have values to give.
			if (valuesList.size() == 0) {
				// No more values left to give, we ran out of coins!
				return null;
			}

			// Get highest value
			int highestValue = valuesList.get(0);

			// Remove highest value from remaining as long as possible.
			while (amountOfMoneyToGiveBack >= highestValue && this.moneyList.getCountByValue(highestValue) > 0) {
				// Remove highest values.
				amountOfMoneyToGiveBack -= highestValue;

				// Add value to give list and remove it from the money list.
				giveList.add(highestValue);
				this.moneyList.removeFromCountByValue(highestValue, 1);
			}
			// No longer able to remove highest value, remove it from the list.
			valuesList.remove(0);
		}
		// Done!
		return giveList;
	}
}
