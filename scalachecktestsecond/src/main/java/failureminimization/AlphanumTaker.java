package failureminimization;

import java.util.ArrayList;

public class AlphanumTaker {
    private ArrayList<Character> charList;
    
    public AlphanumTaker(){
        charList = new ArrayList<Character>();
    }
    
    public void addNewCharToList(char newChar){
        charList.add(newChar);
    }
    
    public boolean listIsValid(){
        boolean listIsAlphaNum = true;
        
        for(int i=0; i<charList.size(); i++){
            if(!charIsAlphanum(charList.get(i)))
                listIsAlphaNum = false;
        }
        
        return listIsAlphaNum;
    }
    
    private boolean charIsAlphanum(char charToCheck){
        if(Character.isDigit(charToCheck) || Character.isLetter(charToCheck))
            return true;
        else
            return false;
    }
    
    public ArrayList<Character> getCharList(){
        return charList;
    }
}