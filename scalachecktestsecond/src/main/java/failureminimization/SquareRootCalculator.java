package failureminimization;

public class SquareRootCalculator {
    public double calculateSquareRoot(int inputNumber){
        return Math.sqrt(inputNumber);
    }
}