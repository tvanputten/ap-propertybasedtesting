package quickchecktestinitial;

/**
 * @author Thomas van Putten
 *	A trivially simple model to demonstrate the difference between property based unit testing and regular unit testing.
 */
public class BasicTestModel {
	private String name;
	private int number;
	
	public BasicTestModel(){
		
	}
	
	public BasicTestModel(String name, int number){
		this.name = name;
		this.number = number;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		if(name.contains("s")){
			this.name = "derp";
		}
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
		if(number > 10){
			this.number = -90;
		}
	}
}
