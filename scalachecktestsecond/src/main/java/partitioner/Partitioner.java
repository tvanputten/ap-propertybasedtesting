package partitioner;

import java.util.ArrayList;

public class Partitioner {
    public static void partition(int number, int max, String prefix, ArrayList<String> result) {
        if ( number == 0 )
            result.add(prefix);

        for ( int partitioner = Math.min(max, number); partitioner >= 1; partitioner-- ) {
            String nprefix = prefix;
            if ( nprefix == "" )
                nprefix = Integer.toString(partitioner);
            else
                nprefix += " + " + partitioner;

            partition(number - partitioner, partitioner, nprefix, result);
        }
    }
}