package exercise

import partitioner.Partitioner

import org.scalacheck.Prop.forAll
import org.scalacheck.{ Gen, Properties }

import scala.collection.JavaConversions._
import scala.collection.mutable

object PartitionerTest extends Properties("Integer Partitioning Test") {
	// http://stackoverflow.com/questions/12640690/recursive-integer-partitioning-algorithm
	def partition(n: Int): Int = {
		def inner(n: Int, max: Int): Int =
			if (n == 0) 1
			else if (n < 0) 0
			else ((1 to max) map (x => inner(n - x, x))).sum

		if (n == 0) 0
		else inner(n, n - 1)
	}

	val partitionValues = Gen.choose(1, 25)

	property("First output test") = forAll(partitionValues) { (n) =>
		val output = new java.util.ArrayList[String]()

		Partitioner.partition(n, n, "", output)

		// IMPLEMENT HERE
		false
	}

	property("Last output test") = forAll(partitionValues) { (n) =>
		val output = new java.util.ArrayList[String]()

		Partitioner.partition(n, n, "", output)

		// IMPLEMENT HERE
		false
	}

	property("Amount of generated summations test") = forAll(partitionValues) { (n) =>
		val output = new java.util.ArrayList[String]()

		Partitioner.partition(n, n, "", output)

		// IMPLEMENT HERE
		false
	}

	property("Summation test") = forAll(partitionValues) { (n) =>
		val output = new java.util.ArrayList[String]()

		Partitioner.partition(n, n, "", output)

		// IMPLEMENT HERE
		false
	}

	property("Duplication test") = forAll(partitionValues) { (n) =>
		val output = new java.util.ArrayList[String]()

		Partitioner.partition(n, n, "", output)

		// IMPLEMENT HERE
		false
	}
}

/*
ABS+m0ihZjv5knflTBe3N8kpAFwEcsy3DtIyEpouiuDs/jKbDpcNWvukEFrSuBdtCKMMl1FrkNXE6tHs
7Hop5TcD4YItz64BgvfJXdKX71GRGgekETpBk0iBHkWandkMPES2gB0qpG9IfCZmZ3jt5dwt9i/uCkL+
ntOg0wZhQxWITWHjWjWXeXY662uK3m6cjB9vrzgJABXHRB8fHmmpTRLlyDYmjhcPt7tyDijwVKShxaIj
xw+cYV9fzDranU2TQE3d67Vc4iFU7X93pLSjIV9A+5pb/DqgscMv8hCG8nNyPHtEASeZdCMQoNIeHYW0
BfMr5wOd4mKvkk35Am7Oh5CrHZ+42hAwtVbno5xeQmO15DstlNmg/HHuvTzMUqXCB+Sdkm7CRlsnxRsZ
1hBk3Ulxipp8QT8rtjRegzwQXIgKJ2UoC4VsrvfTUAJy70EbTzCx61DRA4u6T9ZeXywcm/JfENVM5Brv
OTYrsUCWFzZMB+4MbAvGr8Mi84S6+C/mcvrN9xM0qsEHEh0axFuRSI/s+C/3/WNSmVKvI4q6fcEbjTr6
PqZMJccaIwG9nlH+PZeaX85WxK9IKOUGg1tAKwKIZdt5MXdc/0GU0Azk/6rBY8Ysr7Bpvxvkjr2DVLAg
UOTfxTYQ/e9vpqJ1lYjK+WtWBsg9xiJBThas4MQl2vSvaMWPi0H1Vbr1kylMo2RAscpUuZ3wJQiVvyNj
0g7mR3aHpOzWVVHO8POu0b/wQGzq68mw6Hki5ZfMt6g1nexa1kdDQkA6tyZpA10m4suE6ZcZDndeA/Kd
JvMfrboclrE5XMR3bKfJaamZQoxpKF4YjZE3W/5pG97WV++eSJOAaYjiZbcFaGvNYNQ3RxMSALn5HJ1x
Ye199Exv37NvZS1Ti1XdEKe2UZpmkEdsrrDtdPUQtubmKW2X1EvTfXGSCXGbTfjUh/3krY38eMUi+yzu
CRmpL5AfJrfQMtussxfMDt901zpQrmwMoO3JmQKtPmRq773a3B7Z+hZr4vjxlOBTSNVyw0jDtxQYdEVs
dVk/0873R1HrKEy4yIfrBrptoZG8yjCQV0MhoCgI1EHiehHT92DvZpX6vUm4qSVSG+tKRp7ggPEjL4nw
ahBSxUTYdxJg7HPqGdN6C2wNKO3w9GYiPPsFhjqLeI/ILz/qnn8u+s6VHcRITRb6N6fVVSiyPVjTLnLo
KrmG6I7wH+Erep32eipM/Nwj7Ho9e3iYFLaVK5t2A4E0Ec9jAKyDE+M+EOwbQWrF/6FBmRYEgogtQTG4
iIsThqybzeJ3nlfY3TdfoEainaZVqgDXXQ/Ad+2LCLBAs0qT4k66dsNlUmIqRJnF5U0i5HgH2Pyh2yQK
TDzU2e6Kj3Nn2/FeyZa91cG9pY68P2zfmi75PgoJPpGy/QtacJ9AIM6Q86UiVO5qC7a5OtD+KT88BNC0
SoUbYymORUcQmGh/4TE/yaM5FW1lzpLZUryOZwj7+so8Hi5+oegwaVCRpvf+AetCmEYJfD4eEQnzw+mK
qPwAv+JO/OmxEYjht5hKC0upisV0tpsIFjffiJDTcICRx2f+5vahsctpLTVinf31RiaP5rq9m8oig9mR
QXQmP95FYk3xiDGq7pB0at8O/2MuFfGRI2eyFK9Cnne0GYoM8BdzXhfKFxIetBzDaX745jIjKHZ9j57n
aKgcAGQIKP7bqQESMavEbY3Xg0Z9G/AmbCd8zrmDyNcVwwtcEWsdjN42mXUpp9TU6lPoRQ09ZEinw9p2
qVfjRExJUGDVVTIAtD3NPIjLArk5VYgFyHWUKWttyZNs3DL6CumFR3k/HQ0txGZxUJzbX4Q2TWQkw304
6mtdpRrMGQ40o1polmrRBAdsqBr4Qg4rB3oYzeQ4aGDx6mYfI2Rr3k8GxrkPR9C2bQpeycCzMCqXmBWk
+N62fAG+k3lBai5gtmOuVDZolW6y5SsPxw+oeDRgXB8KYuFfhvuTA0qDsRCHU9l94Rhd6TYIm0GBsfVC
uwVysKVFfzuXweKZpuIcoaw7IBObKxdjtln+HHpTJ5lTTAXkKji4isnL9kjlANIvJPhTGVH8LMCsL666
YdFXX3QVp+6OMNE+EPHxd6sxK8lNPcVaK/itBPoTaHwurQ4oOYmpYEDgnZxtiD/qjakJevowT2L8cf0m
oD7h4uVIsp+TELXZaEBhEQCYxklM8i1zSVHjCOENC2L59TqerNQInwiP9ErJibRYBl+knhJ8XwFiR9DQ
XI03YtOPOREU4e/KB+0mgPODhSPwcmafxNxBwlHLzy2RD1YsQUlHAbk9sY5kbIk3orSe0gFk+K9txgip
PJnbhJKkZqZYV3d6uekU1kRfA6Yogs+ck6pPuXSr7TOrmy3sBVcb8HXjurBJIdRemRVNU8OkPIQVO8zg
S3tWhrLalNV5CygIA5oBvCD6HGQY4ePWuuqWrBvFdA2s0yLyEHkbVsZVQeOrarPtOyip1mbEfjRzDnxQ
JRwcLClUvJzOvYAYSNi4ceT1IMZ6CnGTqt0VKYikdv8AOfrvjUZ8botE4ZLqNRYEhu7k4hH0twL61QJW
2RVbstZax76ekkMZmnNkSl6AcXoWwtZ33sC+BNhJQbRkG/tFfchjstlKPgpJGcRxMVFT1uiH8wICObV4
qSJwSCCT27Omu/8GK/Uek6DgMmM=
*/ 