package exercise

import org.scalacheck.Properties
import org.scalacheck.Prop
import org.scalacheck.Prop._

object ScalaIntroductionTest extends Properties("Conditioning tests") {
	property("conditioning test") = forAll { n: Int =>
		(n >= 0 && n < 10000) ==> ((List.fill(n)("").length == n))
	}

	property("Labeling test case") = forAll { n: Int =>
		false :| "Return value is false" &&
			true :| "This message won't show"
	}

	val falseProp = forAll { n: Int =>
		false
	}

	val trueProp = forAll { n: Int =>
		true
	}

	property("Combining") = falseProp && trueProp
}

/*
d0a4JNO6ROS0HiX1+cHnWgo9fGuZM7zztaHRYjJXTrRK55l2vPtygIvrE/vMy5Zps+DabbZMXjH4zOzo
8VsukQXdZFj+dTriYEyx4wYP1yzRtNQmSaG/SNtSazs56xUEtha75/OX64nl7at0tmceiTImqxpjs6ai
Qe0F2MW5iUR9Sdh6R1SZZZs5DeN11PNVSXPEZt20GmBNmeAkqAo9HI73zVQfXDwV8CJ7phqw0hUYJHig
BwJTcpqgvqASAH1VDP5uJ9IgN9HHIxtbfXTdQvgt+s6YHqbPW2STQKxOBUO2gcTOsUeXo0EBjaDWO678
5WN1JE52w8zJyxKjfd5Jmtmf0NOqTigF3pqULjj7SDqTLnf9bNyIVusoR6QGg0/TzL7RJwhAy+P2evPy
bnffs91+N6KLJX7hheMYY6mDi/D2ixu27st5MXo1RqWlmp411ETZv98XLSO51vMfOBflYqyaA7bzLkeE
kmCqPMj+DltqtUIZe6gdKttTieW4A8YAMRmJn3rQLaBqFmaVlbFyvyGWzWzuAGR7eUz9Gmy5LeVJ0xgN
lkdK7aEjBHFt4oAjtLcqCAHbZ7rREtdiqtJpZZD//cU8roWrHsL+3Lli0L9DtNtEdhZMnvzIMMtOE5CC
R8FVXFqXDje6S3YwEOvGa3vZJNcLNwQjGVBNLXXghPrlYbdoI0YTRgwflH4z/tQaGRnFEGxttJtvF3az
UKwAobiVr1o3iqOOUkvnjzxcbKcRxKdT5XsiyvOrNe9IOuyQgWnUTHl6DAg+nV3IAhxeX4a2T6CJPk8g
gviwQ0fZQ6FR12USI11qKrvj60ugKOdzq0PxiFgmmOfUYmX43VkxXA==
*/ 