package exercise

import generators.NumberTaker;
import org.scalacheck.Prop._
import org.scalacheck.{ Gen, Properties }

//This object will contain generated ints
object FirstGenerator {
	//generatedInts will be a list of numbers between 1 and 10
	val generatedInts: Gen[Int] = for {
		value <- Gen.choose(1, 10)
	} yield (value)
}

object FirstGenTest extends Properties("First Gen Tests") {
	import FirstGenerator.generatedInts

	var numberTaker = new NumberTaker()

	property("Test if a new number will be added in the correct position") = forAll(generatedInts) { (number: Int) =>
		// IMPLEMENT HERE
		false
	}
}

/*
mVtgJTTWNFyCmgGTGHj/0o+YoYWv5RSzrlKWGuyBDRMu/J2/1UExOi5R5hyJBdOZyn0zxzwnWNaio8ZH
foM4+Aj4tgrOQDgpxCgH9/MlWP4bqWG4GaMLzYhjstFamBtPHoqP/gZMkDrsnMjgXd0wtTsDFKthEYrb
1YWMrjtzzTN5S5UTAVwn7C6p5ZJKPM4F1k2lwxg18awVKfw4IvI1KK5zU5dc24iQLiG0Nnc8/zNGQKVM
7WQFPbSF4Qp1YOcCFpIg6JiXtp4IMitUJ/sKDiwRkpxfysgOs7cFZMYAns0fzA5iDrZYww2guX2BIky5
lAY4Y3r95mRvMH0dBZwEr4HXm4gEdU1xAGNLqOVLKBzISFU0j2x4TibQokKIWzRphW6MJjTAnJ93P9LR
vOl5+muPyGq1if9FJjgM8FNUVDxUCFcU15267TBydDIr6knf8FxHsoIBQJVyIMeFBh43q2ErrftTFWDq
WaQTHaPYptKEEKW7JRtpGU955GJILSI0MRnGbx0zMg+Y/CGnZp4fpLDWbuK59BYQbYmbuPBOZC0rqyyn
LnOiWc0878hd5EuCLmMiOwYTofg1S6l/ijG6hx9VHyVjQZ5G2vD6TGBqpemJ0OmIeJbqtAgxKmuK9g5X
YLuLHlx2zlrWC1oe+aAbhqqFvVvgcIUO0VysAwK2rWH188A/nyMlXrhaxGTyFjSG12LBPEB00UVV9jAl
NzEHroBPe8dCySXVEysir71Wy1avzA/eU9d44ctlf5QQES5vs5Tt8v9iP2eOGvS2LRbOSxlx3VZUvjBS
lAwNjBs0Kz1NSeDgQIiNgEIEhnte9Qr/hV7hVr6tnDgaGdgOyac6h+yj6BhmQc6SNpoxCbdtUQ/9cMoK
6h3HJaKosyXSc3OJBae8cpeUY9X3KEe4ErVORk3YOJE0AqB5YCBpz3aQG9ND8iGvedWsNvP/FkDjv5qt
TaIYMLkhJGSR5lDn53kbxg==
*/ 