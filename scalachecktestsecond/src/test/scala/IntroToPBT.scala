package example

import quickchecktestinitial.BasicTestModel
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import collection.mutable.Stack
import org.scalatest._

object IntroToUnitTest extends FlatSpec with Matchers {
	def add(one: Int, two: Int): Int = {
		one + two
	}

	"Add" should "Add two numbers in a basic unit test" in {
		add(1, 2) should be(3)
	}
}

object IntroToPBTTest extends Properties("Intro To Property-Based Testing") {
	property("Basic PBT Test") = forAll { (one: Int, two: Int) =>
		IntroToUnitTest.add(one, two) == one + two
	}
}
