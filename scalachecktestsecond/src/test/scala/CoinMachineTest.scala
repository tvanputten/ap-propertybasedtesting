/**
 * @author Brett Meenink
 * This is the testfile that is used in testing the CoinMachine written by Maurits,
 * through the use of property based testing. It does this by using a generator that
 * generates integer values within a specified range.
 */

package scala

import coinmachine.Machine;
import coinmachine.MoneyList;

import scala.collection.JavaConversions._
import java.util.ArrayList

import org.scalacheck.Prop.forAll
import org.scalacheck.Gen._
import org.scalacheck.{ ConsoleReporter, Prop, Properties, Arbitrary, Gen, Test => SchkTest }

// generator for a valid amount of money
object MoneyGenerator {
	// The style which is actually used for generating custom datatypes.
	val validReturnAmounts: Gen[Int] = for {
		value <- Gen.choose(0, 1390) // Psst
	} yield /*ClassConstructorGoesHere*/ (value)

	// A simple supported datatype generator within the generator object
	val tooBigReturnAmounts = Gen.choose(1391, Int.MaxValue)
}

object CoinMachineTest extends Properties("CoinMachine Tests") {
	//Import external generators
	import MoneyGenerator.validReturnAmounts
	import MoneyGenerator.tooBigReturnAmounts

	// Create localscoped (simple) generators
	val negativeInts = Gen.choose(Int.MinValue, -1)

	//Fix this to not use forAll must be something else
	property("Check correct amount is returned on valid input") = forAll(validReturnAmounts) { moneyToGetBack =>
		val machine = setupFullCoinMachine()
		var sum = 0
		var returnedCoins = machine.giveBack(moneyToGetBack)

		// TODO Uncomment these for the demo.
		// Prints are for debugging purposes
		//println("[CoinMachineTest] Checking if machine screwed us over. We should get: " + moneyToGetBack)
		returnedCoins.foreach { coinValue =>
			sum += coinValue.toInt; // RIGHT
			//sum + coinValue.toInt; // WRONG
			//println("[CoinMachineTest] Added coin with value \'" + coinValue + "\' to sum.")
		}
		//println("[CoinMachineTest] We got: " + sum + ". Number of coins: " + returnedCoins.size())

		sum == moneyToGetBack
	}

	property("Check return null if asked too much") = forAll(tooBigReturnAmounts) { moneyToGetBackWhichIsActuallyTooMuch =>
		val machine = setupFullCoinMachine()
		var returnedCoinsWhichIsActuallyNull = machine.giveBack(moneyToGetBackWhichIsActuallyTooMuch)
		returnedCoinsWhichIsActuallyNull == null
		// Heehee, poor user
	}

	property("Check return empty ArrayList on negative input") = forAll(negativeInts) { negativeAmount =>
		val machine = setupFullCoinMachine()
		var emptyJavaUtilArrayList = machine.giveBack(negativeAmount)
		emptyJavaUtilArrayList.size() == 0
	}

	/**
	 * Creates and returns a coin-machine with the standard amount of coins.
	 */
	def setupFullCoinMachine(): Machine = {
		val moneyList = new MoneyList();

		// Denomination, value, amount
		moneyList.add("1 Cent", 1, 10);
		moneyList.add("2 Cent", 2, 5);
		moneyList.add("5 Cent", 5, 2);
		moneyList.add("10 Cent", 10, 2);
		moneyList.add("20 Cent", 20, 2);
		moneyList.add("50 Cent", 50, 2);
		moneyList.add("1 Euro", 100, 2);
		moneyList.add("2 Euro", 200, 5);
		//                   Total: 1390

		// Create machine
		return new Machine(moneyList);
	}
}