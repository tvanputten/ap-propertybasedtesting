package exercise

import org.scalacheck.Prop._
import org.scalacheck.{ Gen, Properties }

object SortedListTest extends Properties("Sorted List Exercise") {
	property("Test Sorting Function") = forAll { (randomList: List[Int]) =>
		val sortedList = randomList.sortWith(_ < _)

		isListSorted(randomList, sortedList)
	}

	def isListSorted(randomList: List[Int], sortedList: List[Int]): Boolean = {
		// IMPLEMENT HERE
		false
	}
}

/*
y8ghWxp5TbyEcwT4H+zMHajJga40RWRsnuC6WuYHmLMkLaEh5iUuZ9Ez8rIpb/VVKCpgNJeRxiuK6N+Z
i1Re3g208doZUVTdKeYMa1IDtZUV0E2TcI9fARC0bWJHAECnbF57gygLXjAwAOwww7zfDJD1YDdM7pDW
PlYjpggvGr37lNcztub+PrRfYDR/Umqiaj85Wtt0WOmxb1qQ++xAKAeeVadcdL1QgVHGVA5QKgQo17+7
Al/ATWr11EMdyu0N3lubhALJ5yHqJKpr98DqWenLQBzdK4TSXJMZXyQUpnDhDJYMCG1X81l8GouXxDAW
Z64eiKFJn5Qj6uS4ydzCqMfENjJu9tBjQSi83o2bdWrg6uNC5VhN+4jWMVq/gDwLWVQtxR97jgDfpbQ/
U+K1hUaF3O6KQcxHXEHYCpx88PeGCWrx+8O8tLSGwnQh7LFbMqEHy9eIN3mVBMYNPXuyfeKH3o74w5i+
0y9pjnT0WTAvsirHCBgBZ4dkTSmDY4w1gtIrSJuqEVlkVyU4v250t4x35AkVW84ejkgzRE80Wr2ixrY/
xKDtgmeH6Phz7ZQJxNHkDD6T3bHtZSogorarh6xLhYpnhJJj7ccM2pk3tACLFn9cIbfVRfa9o+Yy4NwD
0884fshULg2iQI40rdJ7TTjELSs5YoB9weT4p7JBuFkFz+jG1aspfSlzp1F9SlExW+fJbbf+csvPCCnW
6qV4Gs3wM45vJadn7hyCA7DVNeQrlGrP+Fun8B+QNEwEsAAPNu57phNVi/s1AWV+8F6+pW+1DFL9y/9G
GuPVq5ZGMmgCzmFP2Etr6Zknu22W8pZqrIEQi5g5yIOt4V3wUhceXByk66SL75gEQ+T1MUplAjU4TdwN
cU+HHPF52LAMI6wUv1XCwKN7tB218NCa7bnpTPX2vs7++dBzDDSQD1TFNI2v+xYXbRdaiBkxcJMWKpF+
eBzWNE4p4E48ZunJK5X60RHlayWjpgYpyHGK9Uk0kflAQ2iEUKFBF12HEyevD99RMFlv2OiyKOiAzrWM
cWX/NQWmODlHdFATZ5JBnxvx6AibBsKVyiGW7gNbW+Hb7YEB
*/ 