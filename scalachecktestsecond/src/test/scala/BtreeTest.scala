package exercise

import btree.Node
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object BtreeTest extends Properties("Btree Tests") {
	property("Normal UnitTest") = forAll { (list: List[Int]) =>
		Node.root = new Node(null)

		for (i <- 1 to 50) {
			Node.root.transport(i)
		}

		true == evaluateTree(Node.root)
	}

	property("Property based Test") = forAll { (list: List[Int]) =>
		// IMPLEMENT HERE
		false
	}

	//This function check the validity of a btree
	def evaluateTree(n: Node): Boolean = {
		return evaluateTree(n, Int.MinValue, Int.MaxValue)
	}

	def evaluateTree(node: Node, MIN: Int, MAX: Int): Boolean = {
		if (node == null)
			return true
		try {
			if (node.getLowestValue() >= MIN
				&& node.getHighestValue() <= MAX
				&& evaluateTree(node.getLeftChild(), MIN, node.getHighestValue())
				&& evaluateTree(node.getRightChild(), node.getLowestValue(), MAX))

				return true
			else
				return false
		} catch {
			case e: java.lang.IndexOutOfBoundsException => {
				true
			}
		}
	}
}

/*
NgpdzqUiVWp4tvSj52Kp8SwN+QPafj3md0IePTQEKesnA0DYJry/+wo3KUJvw8isXboBHlTVzANKNMfw
DRN6eH+Dp3822tz8ULmhj3XClcB0Cp+hDSjim9F3YOZLl/2FewTO0/2bNe3FEJGbScewevovAD06hIBr
dGFeQu2nwy2PIvr7drt+MnfH8Q16wO7lqPBgt/+rADvMlg4QrzORwziYAFT4xEV7yanO7gdrJ26hQiev
r56lTmY1RX799F4JRkPrb4q5setbj5WG0Y+X3lEUFlhLncB1NiAL9Q5U+EVcsSSXtqpvlWOxSzJD1Ci3
aTDpI0qkAKpQDXuZ3EX2yCfxXzFggnqDYQo11ohMYDumjVE1M3Terr2o0Url4ftDso5rz6osglReNHjY
XviSdpLt3MZBHp1hwJmfBv71uGbhiDj96HroU/tmXqZa0knBRXhoMTP4vAxlmRcz4U+yVQO41MaFTtm9
+ugu9BlGFaiziLuSRNoZd3kx9tPFQjqjn+4FGNJy4RhzlN3HiPWuxJfUgJQXaSomMiArk8k0rB+eGbBm
gmE1Lt22XNEoq1Vqis++u2e39Jhr9PaQNUnFVsmWKbjtMWlJCHo32ZDiKBGMMjp6OL6uy5mz9jgvH/NF
r7Vwy4noAh55T+xa3d1o02Vl/GE8KgVji5J71rDK/G95SmTs+hCxKgAK1SmiUUhHvrUGs5eZ0yhkHjXR
RAtnFYxBCGqNeTUrJ4/9WbZRwp6uVxeFW/M8EbTJkJGhAD2ZMhosL19Cn0Rei4Bg/e77RzXgLffd78Mk
paMwu5h6Pu2vGBhBfCjkx8YDhb4/zorBpDSjATWVygszHix2Jjxe12nfxyM6DNdFbIrPQXtWAztuO/4a
3C6Ea9osQ/krW6p07EEmMHP0hNlSWscIw+HSqn8TWXbYyRuC/2qQAH2AkT7qu0eGpuVGfj8dRkZvP/i1
vpVyF3pqPUBom+PMlXiIposUR3tB9XRoZh5kA8S9c5SweAiyx5HJITjG+0uWcG+ItFmt28aeatIOLQcH
mDxcgtAzt6HCiA4ZBP/zlw2T9Gnbn8+lANNMxC9dVgYmaORV9xL8c4Q0N3Kzusz2Imivkwl2SuIr49O1
qWReshPBpdighk2FKFqAsFxOEm49UVvbYggVK+OaK8HuibCyXY/9rhV4fKtkXT96CiWnGnlrtgFlLi7a
1fAMvxO3wyDU0rXTSH0kNvYFlhkNK/Xg3/COXSz47yiostL3xhxmGpU7AadjXUCkSPzP8/nDhr7cuDk4
KHGNGtorpA0YVVSA0MyEEuGHg/Z47nf9NL3D2UFq7pbOVA25+XF+Y5JR5Tj8CjfLvC+UIbgEwBsAK0CO
tk6qPDKXnMxT4K5qraEzSRS8Gc5mV78YL6f8VrlMaenb3m5G29mniM7R4eV/vBb/RnqVE/a7enWWbf3J
RHcJ8lzJ2dA96PZip15leBWPQV6T7YJbhlPFNkR7WrS1A+odS6zK4px2JxII6KwAOJ085jHVfyCWJmTV
qYbUwZAT1r26ajtWt8M5cbdw6ZeQIDdkB/f/Hr7iEd5eVsYBfNBoEmla6To=
*/ 