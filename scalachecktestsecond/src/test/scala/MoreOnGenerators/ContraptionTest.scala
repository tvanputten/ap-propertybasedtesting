package exercise.MoreOnGenerators

import moregenerators.Part;
import moregenerators.Contraption;

import quickchecktestinitial.BasicTestModel
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object ContraptionTest extends Properties("Contraption Test") {
	import PartGenerator._

	var contraption = new Contraption()

	// Uncomment the below method and implement them. Also implement the PartGenerator object so the "naam: Part" generation works.

	/*
	property("Add Part To contraption") = forAll { (naam: Part) =>
		false
	}
	*/
}

/*
yInVkWzqavwXaqCnP5R/JLhKl1ib4FJlDlj7991NvI7J+lOsB9sTMtbRCqUN4pMKZf3euv3By9gmz0kU
sFfc6DE2Ya07mieBXP5qpg3sXN8xFFOLAtIIUDROFWyGpEYR3Fc4ygenW7rsWWUz2FJntEDO12UXwXhp
6y24kKmN3HVy1bn8711y+jjZ8Ws24Lq59GMdPBrZ1p8Ex0GzKoV/S1Jdf8TDe2Hi9qVK3XEPW/NnyL2W
YvFGGWJr6s4MWEXpXB6jPS1czViKGr1LgLlTm5tZZE4F45T/EPeQij+Mv1+1kWb1YJ4i0mMyh8v0cm9s
e7exMb+Vy0M9N7eEmwCguWSyodAoMq9rnFAr+BgX5bl8/3GHn07OO3bBRAvA8suo8e2ZAvU6BDnv0SAi
skrvFd9Gdli5HWm1W2PPgB7EwzpP7Lr5nzBzl4Kfr2L75tRnif7A/fydw+wXONIpfO+m7JtBAeoFxTgf
MLppBfGsIotBR6LJTSWNGq0VyCms0w8qz0YNMUyt1a+uy2lOowy5nXKDLwF3hcS4cR9gR4dd9BzuHeK7
zy0vTeDMv5QPxHFUtgNIlYzwMO4a3Z3YgrchR36cqLgkK4Cujcfr3JNaqF7OXgWOp0Bmn5y8QRey1s+S
ju7R2Kz8boHHfzAQGu169z6oKMieJ8iI2MPBYpvtbudhzMr32tVyLUdr3T6efMLDZ60Nuj7v41NDrd1p
QULIgqlC8LzbM50oELNkzRltCmsqoD+acJhKxP+C3vQlLow7SmXWK2vZafn6Oa/iUIRP7VpWtqwCgXQY
28H98fkgXnd+jf0Nm08SUd93zLIJrtdXqUIyuER2eHJ81S7yFnt+On7x+qk597iUPYKh2vthWi6xPR9S
YS8wRI0cYiDgSet6HxaDrHtpeXdZRwFswWjajBQ3MOyGHsSJjq9rx7skR/rIL9eX/zHlFTEud78a7tdy
lXwvWhItPOaK9xXk9f6GJQ==
*/ 