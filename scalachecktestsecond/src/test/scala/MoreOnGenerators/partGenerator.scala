package exercise.MoreOnGenerators

import moregenerators.Part;
import org.scalacheck.Test.Params
import org.scalacheck.{ ConsoleReporter, Prop, Properties, Arbitrary, Gen, Test => SchkTest }
import org.scalacheck.Prop._

/**
 * Generator of case objects for the Rectangle class, as well as an arbitrary generator
 */
object PartGenerator {
	// IMPLEMENT HERE
}

/*
RzYbuh3pGY58IXE5tQuJ5ONe4vKEjleMOQBeiZrzmno1yADi0dTWhagGV7Q286Try8Q5EvNFtql6P4hD
tP/GPaRFnzWa15d+kbz++liKbkjiNgVgVSDK8hBCn27u4ZgrPL7MX8FTuWCt8TRf4dgylmuIKKVIrWmu
/z2UhpKmWkVfRzlXM8r+081IRN3JPmcQOp04EiZm/EFvpb0dIidXwedFXJPYNcbHxlyN6vsLMich02JN
a/PnrOK5PSh/1rsaip9zdkLgstkcyflNn25orRBBl9d/ezIU2+I/AqsFeS+sLV7usyGrNWlldOAJY+xo
Avqsq2/B/YkawXsXxbPlE35cFyc0DSx3undnJSTg6WEYn3kOaQErD0wiBj/DT6YRGlCc36V+BHXakwbq
RFvdpC/EMIleZ7c4kLdSj+LUTCixnrR9g4qlcIhtTyAyzBsbXy2HBtfdFTH66xlmq+kQNeVyyDR+2Qhw
sHk3PZd5JB4CGah6bIGYSV6H0glrFrPC9avCgAWTpuFv0Qd/QfmuxR5gXcjqJaIkxfmWocBDEwxwoEnF
4sppTGPnh6O08JQ46x6Ch+UXApH98rCMnK0NvCREFb1mzs1fGAr5SiAbIq+x9YHtaAc95eGZbyQPrHci
mKS65g0/+X/QgkHLgkmHmNO1aElWXp+Q1aQVfvgvCEZyplFBumxqMT1ljXG5/ar1usfH1ks5sVopC2kH
JWQsg+lYsNTybmNNpNoLwZ2fB5243CZrMqUfHdG5+zrS4rnHH3KcSVzclnfQobJtgkFZ1ztVq5aHdLoi
P/AfUVbCup3F0u7nUpwS1RFSNUrNtqrg4ESQjBHYTfx+8uesEUFs5pgCcppoo5pCWu4CljElZKV3RhRx
pAYWm1v0FNAh89+c
*/ 