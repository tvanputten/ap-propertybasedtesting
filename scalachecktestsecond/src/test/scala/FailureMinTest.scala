package exercise

import failureminimization.SquareRootCalculator
import org.scalacheck.Prop._
import org.scalacheck.{ Gen, Properties }

object FailureMinTest extends Properties("Failure Minimization Tests") {
	var squareRootCalculator = new SquareRootCalculator();

	property("Test when an int is used") = forAll { (generatedInt: Int) =>
		// println("[FailureMinTest] Value tested: \'" + generatedInt + "\'.")
		Math.sqrt(generatedInt) == squareRootCalculator.calculateSquareRoot(generatedInt)
	}
}

/*
dkgRXNJXn6ezZ1RSJ9Z1if1L7HZawRZYFJLVYRtSYCSesbSO18rYOW8sJBJBYDJ2qNOgYHx8bMFOMUcQ
fyt6aXW+Upbmm29RSIqvcKWMiUXG0cjrr4a+1OJ1jE9SRXOygvb6GTSUKxC7x2R8AOVttkaCZCcpBH2S
U5/x8utYNVyNK2pPsejcsyyfDnquJkYdmDAUJuV0HnFvwCkckzHteuEVBkidSL2OKvm8JlxNAumyHAYE
njFuJUi+NJg04kWavf8b63sMN+0CCpsH9cbKvwW0Z0880W8k5Jy5nhMzHKUo0P6ss4C4lZBz2yZ4a328
ftWRACZJYgpPUewSJWd4oEU1bG2WAOW6rYwtf8kAnLDaaa98LMh5CAlm7jXPyka5jP2KFfrVXAQOD5yr
Le8ffRnSMciIv731Lxv7tTpx1cX/ISSYwZEPJIfpHTOGb0BSrjHD1awrlVgo7PfJ9RhYNIh5fH62O73C
MB16zEMzSBfRLeboG2GGLE+LhIPwID/2gqxohM0U8D/ZjiooA9vZi22PN8Qa/MEpQSqR7FqnIGqmT2JG
UEzk0Dmxey0cTpEDKv3E2datml1ZxXIAEsjSTtSbRDzo5kqrocA5D6zstb4tK9X7QLR6MRl3vtSD/xg7
8NK7UOIfmJ5I//Hc3whxUdnHKhNDytH4WDn5ZlSxve9Qh7sLftxnmjps8Py3jO0FkueVXSL400/bijoz
HvBDXmanZwiEJKaV0/wcxfYD9pupIw3Jyv29l/M6sQj+q2hBq720m7bSA/t8kraHSXpGmvdTr9c2Sw98
0/fqTB1miWqsh0F0MqSEwJLwPfcF3kGS56M+9lf8kYgB7SfTbgv0Jw==
*/ 