name := """PropertyBasedTestingexample"""

organization := "example"

version := "0.1.0-SNAPSHOT"

//scalaVersion := "2.11.2"

//crossScalaVersions := Seq("2.10.4", "2.11.2")
autoCompilerPlugins := true

libraryDependencies ++= Seq(
  "org.scalacheck" %% "scalacheck" % "1.9" % "test",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test"
)

testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-maxSize", "5", "-minSuccessfulTests", "33", "-workers", "1", "-verbosity", "1")

initialCommands := "import example._"

