This is a an [activator](https://typesafe.com/activator) template for demoing a how to use property based testing on Java with Scala and [ScalaCheck](http://www.scalacheck.org/)

Warning: This project only builds with sbt and does NOT have maven support

Uses:
* sbt 0.13.5
* Cross build against Scala 2.10.4, 2.11.2
* [ScalaTest](http://www.scalatest.org/)
* [ScalaCheck](http://www.scalacheck.org/)

Notes:
* In the typesafe activator test view tests wil only mark as failed when one test fails like a lot so pay attention to the number indicator next to the tests view.
