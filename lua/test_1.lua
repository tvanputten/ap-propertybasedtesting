function table.count(tbl)
	local count = 0

	for k, v in pairs(tbl) do
		count = count + 1
	end

	return count
end

function printTable(t, indent, done)
	done = done or {}
	indent = indent or 0
	for k, v in pairs(t) do
		io.write(string.rep(" ", indent))
		
		if type(v) == "table" and not done[v] and k ~= "owner" then
			done[v] = true
			
			io.write(tostring(k) .. ":" .. "\n")
			
			printTable(v, indent + 4, done)
		else
			io.write(tostring(k) .. " = ")
			io.write(tostring(v) .. "\n")
		end
	end
end

function generate(args)
	if args.dataType == "string"  then
		local randomString = "";

		for i = (args.minLength or math.random(-math.pow(2, 32), -1)), args.maxLength or math.random(0, math.pow(2, 32)) do
			randomString = randomString .. string.char(math.random(65, 122))
		end

		return randomString
	elseif args.dataType == "number"  then
		return math.random(args.min or 0, args.max or 1024)
	else
		error("unknown dataType: " .. args.dataType)
	end
end

function determineComplexity(value)
	if(type(value) == "number") then
		return math.abs(0 - value);
	elseif(type(value) == "string") then
		return string.len(value);
	end
end

function executeTest(data)
	local startTime = os.clock()

	local results = {}

	while(true) do
		-- Generate random data
		local arguments = {}

		local status, err = pcall(function()

			for index, testMethods in pairs(data.inputArgs) do
				local randomTestMethod = testMethods[math.random(1, table.count(testMethods))]

				table.insert(arguments, generate(randomTestMethod))
			end

			-- Execute input function.
			data.inputFunc(unpack(arguments))
		end)

		table.insert(results, {
			success = status,
			err = err,
			data = data,
			arguments = arguments,
			complexity = 0
		})

		if os.clock() - startTime > data.maxTime  then
			print("Maximum test time reached!")

			break
		end
	end

	return results
end

function minimize(results)
	local leastComplexFail;
	local mostComplexSuccess;

	-- Determine complexity
	for _, resultData in pairs(results) do
		for _, arg in pairs(resultData.arguments) do
			resultData.complexity = resultData.complexity + determineComplexity(arg)
		end
	end

	-- Sort by success and complexity
	table.sort(results, function(a, b)
		if(a.success == b.success) then
			return a.complexity < b.complexity
		else
			return a.success
		end
	end)

	for k, v in pairs(results) do
		if(not v.success and not leastComplexFail) then -- Look for the first not successful
			leastComplexFail = v
		elseif(v.success) then -- Look for the latest successful
			mostComplexSuccess = v
		end
	end

	return leastComplexFail, mostComplexSuccess
end

function startPbt(data)
	print("Executing test: ")
	printTable(data)
	print("\n\n")

	local results = executeTest(data)
	local leastComplexFail, mostComplexSuccess = minimize(results)

	print("Most complex success: ")
	printTable(mostComplexSuccess.arguments)
	print("\n\n")
	print("Least complex fail: ")
	printTable(leastComplexFail.arguments)
end

function testme(someString, numOfTimes)
	if numOfTimes < 0  then
		error("cannot repeat a negative times")
	end

	-- Repeat a string multiple times.
	return string.rep(someString, numOfTimes)
end

startPbt({
	inputFunc = testme,
	inputArgs = {
		{
			{dataType = "string", minLength = 8, maxLength = 20}
		},
		{
			{dataType = "number", min = -10000, max = 10000}
		}
	},
	maxTime = 0.25 -- Increase to improve accuracy!
})

-- TODO: For a dynamically type language like Lua, how do we figure out the datatype of a function?


function linearTest(num) --O(n)
	for i = 0, num, 1 do end
end

function exponentialTest(num) -- O(n^2)
	for i = 0, num, 1 do for j = 0, num, 1 do
	end end
end

function cubedTest(num) -- O(n^3)
	for i = 0, num, 1 do for j = 0, num, 1 do for k = 0, num, 1 do
	end end end
end

function testLength(fn, n)
	local startClock = os.clock() -- Begin meeting aantal processortijd.
	fn(n) -- Voer functie uit.
	return os.clock() - startClock -- Pak het huidige aantal processortijd en trek eerder gemeten aantal af.
end

print("linearTime", testLength(linearTest, 2000))
print("exponentialTime", testLength(exponentialTest, 2000))
print("cubedTime", testLength(cubedTest, 2000))
